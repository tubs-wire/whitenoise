function  [W, Winc, sqrt_lambda, f, t, N, h]=do_wiener_vector_kl(m,tspan,n_inst, n_wiener, discretization, rv)
T = tspan(end);
switch(discretization.mode)
    case('h')
        h = discretization.value; 
        N = ceil(T/h)+1;
        t = 0:h:T;  
    case('N')
        N = discretization.value + 1; 
        h = T/(N-1);
        t = 0:h:T; 
    case('t')
        t = discretization.value; 
        N = length(t); 
        h = T/(N-1); 
    otherwise
        error('DISCRETIZATION MODE ERROR: Discretization mode unknown');
end


if ( (  ( t(1) ~= tspan(1) ) || ( t(end) ~= tspan(end) ) ) )
        error('The time span and t vector do not match');
end

length_t = length(t);
W = zeros(length_t, n_inst, n_wiener);

if(tspan(1) == tspan(2))
    return
end
if (~exist('rv'))
  Z = randn(m,n_inst, n_wiener);
else
    Z = rv;
end
T = tspan(end);
[M,X] = meshgrid([1:m], t );
sqrt_lambda = feval(@(i)(2*T)./((2*i-1)*pi), M);
f = feval(@(x,i)sin(x./( (2*T)./((2*i-1)*pi) )), X, M);
for i = 1:n_wiener
   W(:,:,i) = f.*sqrt_lambda*Z(:,:,i);
end
Winc = zeros(N-1, n_inst,n_wiener);
for i = 2:N
    Winc(i-1,:,:) = W(i,:,:)-W(i-1,:,:);
end