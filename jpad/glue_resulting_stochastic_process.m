function x_sgm = glue_resulting_stochastic_process(xi, theta, alpha, x0);
%GLUE_RESULTING_STOCHASTIC_PROCESS puts together the PCE coefficientes XI, 
%and the hermite polynomials given by the multiindices ALPHA evaluated at 
%the random variables THETA.

% rows_theta = n_rv*n_wiener. cols_theta = n_inst
[rows_theta,cols_theta] = size(theta);

% rows_alpha = number of multiindices. cols_alpha = n_rv*n_wiener 
[rows_alpha, cols_alpha] = size(alpha);

%rows_xi = number of multiindices. cols_xi = n_step
[rows_xi,cols_xi] = size(xi);

if(rows_theta ~= cols_alpha)
     error('random vector and multiindices do not match ');    
end

if (rows_xi ~= rows_alpha)
    error('the number of pce coefficient do not match the number of multiindices');
end

if (cols_xi == 1) 
     xi = reshape(xi, 1, []);
     cols_xi = size(xi, 2);
end

rows_xsgm =cols_alpha;
cols_xsgm = cols_xi;
rows_xsgm = cols_theta;
x_sgm = zeros(cols_xsgm, rows_xsgm);

H_alpha = zeros([rows_alpha cols_theta]);
H_alpha_i = ones([1 cols_theta]); 
for k = 1:rows_alpha
    H_alpha_i = ones([1 cols_theta]); 
    for j = 1:cols_alpha
        h = hermite( alpha(k,j) );
        H_alpha_i = H_alpha_i.*polyval(h, theta(j,:));
    end
    H_alpha(k,:) = H_alpha_i;
end
x_sgm = xi'*H_alpha;