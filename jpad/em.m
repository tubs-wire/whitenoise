function x_new = em( a, b, h, t ,dW, x_old, n_inst, n_wiener )
% EM Euler Maruyama method for SDEs 
% X_NEW = EM(A, B, DT, T, DW, X_OLD, N_INST) computes  the numerical 
% solution of the SDE dX = Adt + BdW, X,  using the Euler-Maruyama  at step T 
%
% SEE ALSO MILSTEIN, SDE_CLASSIC_SOLVER
%
sum_b = zeros(size(x_old));
for i = 1:n_wiener
    sum_b = sum_b + feval(b{i},t,x_old).*dW(:,:,i);
end
x_new = x_old + feval(a,t,x_old)*h + sum_b;

