function b = build_diffusion_coefficients(g, ode_fnc, sde_int)
% BUILD_DIFFUSION_COEFFICIENTS builds the diffusion of a scalar SDE B 
% B = BUILD_DIFFUSION_COEFFICIENTS(G, LIN_MULT, MILSTEIN) returns a cell
% array of the size of g containing the diffusion coefficient of the SDE 
% dX = A(t,X_t)dt + B^j(t,X_t)dW^(j) depending on the type of linear noise
% given in lin_mult. If lin_mult is true, then the SDE  contains linear
% homogeneous multiplicative noise, otherwise the noise is linear additive.
% Additionally, the values of B'^j(t,X_t) are stored in the second row of b
% for the case that the Milstein scheme is used to solve the SDE. 
%
% EXAMPLE
%
% lin_mult = true; milstein = true; g = {@(t) t, @(t) cos(t)} b =
% build_difussion_coefficients(g, lin_mult, milstein)
% 
% SEE ALSO SDE_SGM_FRAMEWORK
%
switch(sde_int)
    case 'em'
        milstein = false;
    case 'milstein'
        milstein = true;
    otherwise
        error ('Wrong sde solver. Choose between em or milstein');
end
switch (ode_fnc)
    case 'lin_add_1d'
        lin_mult = false; 
    case 'lin_mult_1d'
        lin_mult = true;
    otherwise
        error ('Wrong ode_fnc. Only option are lin_add_1d or lin_mult_1d')
end
        size_g = size(g, 2);
        if(milstein)
            b = cell(2,size_g);
            if(lin_mult)
                for i = 1 : size_g
                    b{2,i} = @(t,x)feval(g{i},t);
                end
            else
                for i = 1 : size_g
                    b{2,i} = @(t,x) 0;
                end
            end
        else
            b = cell(1, size_g);
        end
            if(lin_mult)
               for i = 1:size_g
                  b{1,i} = @(t,x)feval(g{i},t).*x;
               end
            else
               for i = 1:size_g
                  b{1,i} = @(t,x)feval(g{i},t);
               end
           end
        end