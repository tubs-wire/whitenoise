function demo_examine_wiener

m = 100;
n_inst  = 200;
n_sigma = 3;
[t,W, sqrt_lambda, f]=do_wiener_vector_kl(m, [0,1],n_inst);
[avr_pnkt, sigma_sq_pnkt] = examine_wiener(W,t);
stat = avr_pnkt*ones(1,2*n_sigma+1)+sqrt(sigma_sq_pnkt)*[-n_sigma:n_sigma];
length_t = length(t);
C = zeros(length_t,length_t);
for i = 1:length_t
    for j = i:length_t
        C(i,j) = sum(sqrt_lambda(1,:).^2.*f(i,:).*f(j,:));
    end
end
C = C+C'-diag(diag(C));

subplot(1,2,1)
hold on
plot(t,W)
plot(t, stat,'k','LineWidth',2)
xlabel('Time')
hold off
title('One dimensional Wiener process with first and second moments')
[T,S] = meshgrid(t);
subplot(1,2,2)
contour(T,S,C)
title('Autocovariance of the Wiener process')
xlabel('t', 'FontSize', 10)
ylabel('s', 'Rotation', 0, 'FontSize',14)
