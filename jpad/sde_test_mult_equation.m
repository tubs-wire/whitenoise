function [t,y] = sde_test_mult_equation(a, n_step, n_inst, T, x0, W)
%SDE parameters
b = 1; 
%dX_t = aX_tdt + bX_tdW_t
% X_t = X_0exp((a-0.5b^2)*(t+W))
n_wiener = 1; 
%discretization.mode ='N'; discretization.value = partition_size;
t = linspace(0,1,n_step);
%[W,dW,t, N, h] = do_wiener_vector_mc(T,n_inst, n_wiener ,discretization, test);
tt = repmat(t',1,n_inst);
y = x0*exp((a-0.5*b^2)*(tt)+b*W);
%y = [repmat(x0, 1, n_inst); y];
if(nargout < 1)
  figure(3)
  plot(t, X_t)
  title('Exact solution')
end