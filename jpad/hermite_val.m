function y=hermite_val( pce_coeff, x )
% HERMITE_VAL Evaluate expansion in Hermite polynomials.
%   Y=HERMITE_VAL( PCE_COEFF, X ) evaluates the Hermite polynomial given
%   by the coefficients in PCE_COEFF at the positions given in X. 
%
% Example
%   pcc=pce_expand_1d( @exp, 4 );
%   x=linspace(0,10);
%   y=hermite_val( pcc, x );
%   plot(x,y);
% 
% See also HERMITE_VAL_MULTI

%   Elmar Zander
%   Copyright 2006, Institute of Scientific Computing, TU Braunschweig.
%   $Id: hermite_val.m,v 1.3 2006/10/16 13:18:24 ezander Exp $ 

p=[];
for i=1:length(pce_coeff)
    h=hermite(i-1);
    p=[0 p]+pce_coeff(i)*h;
end

y=polyval(p,x);
