function [I_f,err] = ito_integral_kl(intfnc, tspan)
% ITO_INTEGRAL_KL evaluates the Ito Integral using Wiener Processes via KL 
    m = 100;
    [t,W] = do_wiener_vector_kl(m,tspan);
    f = feval(intfnc, t);
    % I_f = SUM(from i=1, to n-1) f(t(i))*( W(t(i+1)) - W(t(i) )
    I_f = sum(f(1,end-1).*W(2:end)) - sum(f(1,end-1).*W(1:end-1));
    if( nargout > 1 )
        err = abs(I_f -0.5*W(end)^2-t(end));
    end