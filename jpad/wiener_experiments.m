function wiener_experiments
clc
figure('Position', [1,1, 1234,700])
tspan = [0,1];
n_inst = 1; 
n_wiener = 1;
base = 10;
L = 5;
grb_factor = 100; 
test = false;
discr_ref.mode = 'N'; discr_ref.value = base^L;
[Wref, dWref, tref, n_step, href]  = do_wiener_vector_mc(tspan(end) ,n_inst, n_wiener, discr_ref, test);
idx = 1:grb_factor:length(tref);
t_hat = tref(idx); 
W_hat = Wref(idx,:,:);
discr_hat.mode = 't'; discr_hat.value = t_hat;
for i = 2:size(W_hat, 1)
    dW_hat(i-1,:,:) = W_hat(i,:,:) - W_hat(i-1,:,:);
end

m = [10 100 1000 10000];
err = zeros([1 length(m)]);
for i = 1:length(m)
    rv_sgm = build_sgm_rv(dW_hat,m(i),n_wiener,t_hat);
    %rv_sgm = build_sgm_rv(Wref_inc, m, n_wiener, t);
    [Wkl, dW_kl, sqrt_lambda, f, tkl, N, h]=do_wiener_vector_kl(m(i),tspan,n_inst, n_wiener, discr_hat, rv_sgm);
    %subplot(2,2,i)
    %plot(t_hat, W_hat)
    %hold on
    %plot(tkl, Wkl,'g')
    %title_str = sprintf('m = %d', m(i));
    %hold off
    %title(title_str)
    %xlabel('Time')
    %ylabel('Wiener Processes')
    err(i) = simpson_rule((abs(W_hat-Wkl).^2)); 
end
%waitforbuttonpress();
%print -depsc ...
%    ../../../../latex/document/images/results/wiener_experiments_2.eps
if 1
  %figure(2) 
  %figure('Position', [1,1, 1234,700])
  subplot(2,2,1)
  plot(t_hat,W_hat)
  title('Stepwise approximation')
  xlabel('Time')
  ylabel('W_{hat}')
  subplot(2,2,2)
  plot(tkl, (W_hat-Wkl).^2, 'r.-')
  title('(W_{hat} - W_{kl})^2')
  xlabel('Time')
  ylabel('(W_t - W_{kl})^2')
  subplot(2,2,3)
  plot(tkl, Wkl, 'g')  
  title('KL using transformed variables')
  xlabel('Time')
  ylabel('W_{kl}')
  subplot(2,2,4)
  plot(t_hat, W_hat)
  hold on
  plot(tkl, Wkl,'g')
  hold off
  title('Stepwise approximation and transformed')
  legend('Stepwise approx.', 'Transformed', 'Location', 'SouthEast')
   xlabel('Time')
   ylabel('Wiener Processes')
 %  print -depsc ...
 %   ../../../../latex/document/images/results/wiener_experiments.eps
    waitforbuttonpress();
   % figure(3)
   % figure('Position', [1,1, 1234,700])
end
   loglog(m,err,'-o')
   xlabel('log(m)')
   ylabel('log(err)')
   title('Error plot between the stepwise approx. and KL as a function of the number of random variables ')
   %print -depsc ...
   % ../../../../latex/document/images/results/wiener_experiments_error.eps