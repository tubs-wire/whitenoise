function I_alpha_k= build_alpha_k(alpha)
% BUILD_ALPHA_K computes the alpha_k = alpha - e_k 
%
% I_ALPHA_K= BUILD_ALPHA_K(ALPHA) returns the indices of the multindices matrix ALPHA such that alpha_k = ALPHA - e_k is found in ALPHA. This function is used in building the ODE corresponding to linear multiplicative noise.   
%
% EXAMPLE
% p = 3; m = 2; 
% alpha = multiindex(p, m); 
% I_alpha_k = build_alpha_k(alpha);
%

%sizeof_Ialpha = length(alpha);
%n_rv = size(alpha, 2); 
%temp = [];
%p_idx = 0;
% Build the enhanced matrix alpha - e_k
% For every rows in alpha (alpha(i,:)) compute 
% alpha-e_k (alpha(2:n_rv+1,:))
    m = size(alpha, 2);
    ld = size(alpha, 1); 
    i = 1:m*ld;
    z = reshape(i, ld, []);
    I = reshape(z', 1, []);
    temp2 = repmat(alpha, m,[]);
    alpha2 = temp2(I, :);
    e = repmat(alpha(2:m+1, :), ld, 1);
    temp = alpha2 - e;

idx = zeros(length(temp),1);
for i = 1: length(temp)
% find  indices that actually exist
    p_idx = multiindex_find(alpha, temp(i,:));
    find_pidx = find(p_idx);
    if  (isempty(find_pidx) )
       I_alpha_k(i) = 0; 
    else
       I_alpha_k(i) = find_pidx;
    end 
end