function y = kronecker_delta(i,j, out_size)
%KRONECKER_DELTA computes the delta kronecker 
%Y = KRONECKER_DELTA(I, J, OUT_SIZE) returns a matrix of 1's of size
%OUT_SIZE  if i equals j otherwise it returns a matrix of 0's of size
%OUT_SIZE. If OUT_SIZE is not provided it is set by default to 1.
%EXAMPLE
% I = rand(5); J = rand(5)
% Y =  KRONECKER_DELTA(I, J)
if (exist('out_size') ~= 1)
  if( (i - j) == 0 )
      y = 1; 
  else
      y = 0; 
  end
else
    if( (i - j) == 0 )
       y = ones(out_size); 
    else
       y = zeros(out_size);
    end
end
