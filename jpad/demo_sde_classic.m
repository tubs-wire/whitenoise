function demo_sde_classic

clc
test = false;
sde_int = 'em'; 
ode_fnc ='lin_add_1d';
n_inst = 1; 
n_step = 10e3;
G = [10e2]; 
n_wiener = 1;
x0 = randn(); 
tspan = [0,1];
l = -1; % Parameter of the exact solution
G0 = @(t) l; 
g0 = @(t) 0; 
g = {@(t) 1};
a = @(t_i, x_i) feval(G0,t_i).*x_i + feval(g0, t_i);
b = build_diffusion_coefficients(g, ode_fnc, sde_int);
switch(sde_int)
    case ('em')
        sde_int_handle = @em;
    case('milstein')
        sde_int_handle = @milstein;
    otherwise
       error('SDE_INT ERROR: Option of sde_int not valid. Choose betweem em or milstein');
end
discr.mode = 'N'; discr.value = n_step;
[Wref, dWref, tref, Nref, href]  = do_wiener_vector_mc(tspan(end) ,n_inst, n_wiener, discr, test);      
[t_ref, x_ref] = sde_test_manager(l, Nref, n_inst, tspan(end), x0, ode_fnc, dWref, Wref);
err = zeros([length(G), n_inst]);
for k = 1:length(G)
     grb_factor= G(k);
    idx = 1:grb_factor:length(tref);
    t_hat = tref(idx); 
    W_hat = Wref(idx,:,:);
    discr_hat.mode = 't'; discr_hat.value = t_hat;
    n_step_hat = size(W_hat, 1);
    for i = 2:n_step_hat
        dW_hat(i-1,:,:) = W_hat(i,:,:) - W_hat(i-1,:,:);
    end
    [t_cla,x_cla] = sde_classic_solver(sde_int_handle, a, b, x0, n_step_hat, n_inst, n_wiener, dW_hat,t_hat);
    [t_hat, x_hat] = sde_test_manager(l, n_step_hat, n_inst ,tspan(end), x0, ode_fnc, dW_hat, W_hat);
    plot(t_cla, x_cla, t_hat, x_hat)
    %subplot(2,2,k)
    %plot(t_cla, x_cla,'r', t_ref, x_ref, 'b', t_hat, x_hat, 'k')
    %xlabel('Time')
    %ylabel('X_t')
    %legend('X_{EM}','X_{REF}', 'X_{ANA}')
    %err(k,:) = simpson_rule(abs(x_cla - x_hat).^2);   
end