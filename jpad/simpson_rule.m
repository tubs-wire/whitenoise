function I_f = simpson_rule(f)
% SIMPSON RULE calculates the integral of function f using the Simpson rule
% 
N = size(f, 1);
h = 1/(N-1);
I_f = (h/3)*(f(1,:)+f(N,:) + 4*sum(f(2:2:N-1,:))+ 2*sum(f(3:2:N-1,:)));