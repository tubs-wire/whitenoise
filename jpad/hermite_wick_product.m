function [Z_gamma,I_Z]=hermite_wick_product(X_alpha, I_X, Y_beta, I_Y, I_Z )
%   HERMITE_WICK_PRODUCT Wick product of two multivariate Hermite polynomials 
%   [Z_GAMMA,I_Z]=HERMITE_WICK_PRODUCT( X_ALPHA, I_X, Y_BETA, I_Y ) does
%   the Wick product between X_ALPHA (with corresponding multiindex set
%   I_X) and Y_BETA (with corresponding multiindex set I_Y) to produce the
%   PC expanded random variable Z_GAMMA. The multindex set I_Z is generated
%   such that all nonzero terms can be represented.
%   [Z_GAMMA]=HERMITE_WICK_PRODUCT( X_ALPHA, I_X, Y_BETA, I_Y, I_Z ) uses
%   the supplied multiindex set I_Z and computes only product terms
%   referred to in this set.
% Example
%   N=10; m=3; p_X=2; p_Y=4;
%   I_X=multiindex(m,p_X); X_alpha=ones(N,size(I_X,1)); 
%   I_Y=multiindex(m,p_Y); Y_beta=ones(N,size(I_Y,1)); 
%   [Z_gamma,I_Z]=hermite_wick_product( X_alpha, I_X, Y_beta, I_Y );
%

% check number of arguments
nargchk( 3, 5, nargin );

% assume arguments
if nargin<4
    I_Y=I_X;
end

% check whether pc variables are specified correctly
if size(X_alpha,2)~=size(I_X,1)
    error( 'pc variable X_alpha and multiindex set I_X don''t match in pce_multiply' );
end
if size(Y_beta,2)~=size(I_Y,1)
    error( 'pc variable Y_beta and multiindex set I_Y don''t match in pce_multiply' );
end
if size(I_X,2)~=size(I_Y,2)
    error( 'multiindex sets I_X and I_Y don''t match in pce_multiply (different number of gaussians)' );
end

% compute the result multiindex set if necessary
ord_X=max( multiindex_order(I_X) );
ord_Y=max( multiindex_order(I_Y) );
if nargin<5
    ord_Z=ord_X+ord_Y;
    I_Z=multiindex( size(I_X,2), max( ord_Z ) );
end
I_min_col = min(size(I_X, 1), size(I_Y,1)); 
I_max_col = max(size(I_X, 1), size(I_Y,1));
I_xPlusY= I_X(1:I_min_col,:) + I_Y(1:I_min_col,:);
rows_I_Z = size(I_Z, 1); 
n = size(X_alpha, 1); 
Z_gamma=zeros(n,rows_I_Z);
if(size(I_X, 2) > 1)
   for i = 1:I_min_col
       temp = I_Z - repmat(I_xPlusY(i,:),rows_I_Z, []); 
       Z_gamma(find(temp == 0)) =  1;
   end
else
  for i = 1:I_min_col
      temp = I_Z - I_xPlusY(i)*ones(size(I_Z)); 
      Z_gamma(find(temp == 0)) =  1;
  end
end

if 0
n = size(X_alpha, 1); 
Z_gamma=zeros(n,size(I_Z,1));

for j=1:size(I_X,1)
    alpha=I_X(j,:);
    for k=1:size(I_Y,1)
        beta=I_Y(k,:);
        aPlusB = alpha + beta; 
            for i=1:size(I_Z,1)
                gamma=I_Z(i,:);
                Z_gamma(:,i)=  kronecker_delta(gamma,aPlusB,[n,1]);
            end
    end
end
end