function [T,thrd_Q] = reshape_rv(Q, mode)
% RESHAPE_RV reshape 3-D matrix into a 2-D Matrix according to MODE. 
% 
% T = RESHAPE_RV(Q,MODE) reshape the 3-D matrix Q into a 2-D matrix T
% according to MODE. If MODE = 'U' then T is filled by first looping in the
% 3th dimension and then in the 1st. If MODE = 'D' the loop runs first on
% the 1st dimension and then in the 3rd one. 
% 
% [T, THRD_Q] = RESHAPE_RV(Q,MODE) returns the 3rd dimension of Q neccesary
% to inverse the effect of reshape.
%
% This function is used to reorganize the random variables (rv). For a set
% of set of rv q = {q_i^j}. Mode 'U' reorganizes q as q_1^1,
% q_2^1,...,q_n^1, q_1^2, q_2^2,..., q_n^2, ...,q_1^d, q_2^d,..., q_n^d.
% MODE 'D' reorganizes as q_1^1, q_1^2,...,q_1^d, q_2^1, q_2^2,..., q_2^d,
% ...,q_n^1, q_n^2,..., q_n^d. 
%
% EXAMPLE
% Q = randn(3,2,4); 
% MODE = 'U'; 
% T = reshape_rv(Q, mode)
%
% SEE ALSO INV_RESHAPE_RV
%
rows_Q = size(Q,1); 
cols_Q = size(Q,2); 
thrd_Q = size(Q,3); 

T = zeros(rows_Q*thrd_Q, cols_Q);
j = 1;
switch (mode)
    case ('U')
        for k = 1:thrd_Q
            for i = 1:rows_Q
                T(j,:)= Q(i,:,k);
                j = j+1; 
            end
        end
    case('D')
        for i = 1:rows_Q
            for k = 1:thrd_Q
                T(j,:) = Q(i,:,k);
                j = j+1;
            end
        end
    otherwise
        error('MODE ERROR: incorrect mode entered. Choose between U or D');
end
