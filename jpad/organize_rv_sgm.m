function theta_p = organize_rv_sgm(theta, indices, m)

k = 1; 
theta_p = zeros(size(theta));
for i = 1:length(indices)
    idx_j = 0; 
    idx_i = i;
    for j = 1:length(m)
        theta_p(idx_i+idx_j,:) = theta(k,:);
        idx_j = m(j) + idx_j; 
        k = k+1;
    end
end