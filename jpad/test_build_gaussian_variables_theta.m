function test_build_gaussian_variables_theta

assert_set_function('build_gaussian_variables_theta');
m = 3; 
tspan = 0:1/3:1;
assert_equals(build_gaussian_variables_theta([1 0 0 0],tspan,m),[0.120619;0.30001054387;0.3360002623],'RV_SGM_1','reltol',10-5 )
assert_equals(build_gaussian_variables_theta([0 1 0 0],tspan,m),[0.32953864; 0.3001054; -0.245970991],'RV_SGM_2','reltol',10e-5 )
assert_equals(build_gaussian_variables_theta([0 0 1 0],tspan,m),[0.450158158; -0.300105439;0.090003163161571],'RV_SGM_3','reltol', 10e-4 )
assert_equals(build_gaussian_variables_theta([0 0 0 1],tspan,m),[0;0;0],'RV_SGM_4')