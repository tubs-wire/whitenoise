function Q = transformation_matrix_rv(M, h)
%TRANSFORMATION_MATRIX_RV transformation rv of classical to pce method-
%
%  SEE ALSO DO_WIENER_VECTOR_MC
%
%  EXAMPLE 
%
%  m = 4; % number of pce variables
%  t = linspace(0,1,7); % times of classical method 
%  disc.mode = 't'; 
%  disc.value = t;
%  n_inst = 1; T = 1; 
%  W_mc = do_wiener_vector_mc(T, n_inst, disc);
%  Q = transformation_matrix_rv(m,t); 
%  W_pce = Q*W_mc; 
%

I = h:h:1; 
K = 1:M; 
Q = sqrt(2)*cos((I'*(K-0.5)*pi)');
if 0
    length_t = length(t);
    T = t(end); 
    Q = zeros(m, length_t-1); 
    for j = 1:m
        sqrt_lambda_j = 2*T/((2*j-1)*pi);
        for i = 1:length_t-1
            Q(j,i) =  sqrt(2/T)*sqrt_lambda_j*(cos((t(i)/sqrt_lambda_j))-cos(t(i+1)/sqrt_lambda_j));
    %         Q(j,i) =  sqrt(2/T)*sqrt_lambda_j*(cos((t(i)/sqrt_lambda_j)));
        end
    end
    if 0
    for j = 1:m
        sqrt_lambda_j = 2*T/((2*j-1)*pi);
        Q(j,end) =  sqrt(2/T)*sqrt_lambda_j*cos((t(end)/sqrt_lambda_j));
    end
    end 
end