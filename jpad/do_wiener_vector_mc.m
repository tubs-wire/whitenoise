function [W,dW,t, N, h] = do_wiener_vector_mc(T,n_inst, n_wiener ,discretization, test)
%   DO_WIENER_VECTOR_MC creates a Wiener process using Montecarlo methods.
%   W = DO_WIENER_VECTOR_MC(T,N_INST,N_WIENER ,DISCRETIZATION, TEST) creates a matrix with
%   N_INST columns of Wiener processes, each column being independent from
%   each other. N_WIENER is the number of wiener process .The Wiener process is specified on 0 <= t <= T. There are 3
%   ways of discretizing the time . DISCRETIZATION is a data structure with 
%   2 fields. DISCRETIZATION.mode which is a character 'h', 'N', or  't'
%   and DISCRETIZATION.value. If the MODE field is 'h', when the time
%   discretization is taken using a step size 0 <= h < 1. If the MODE field
%   is 'N', then the time is discretizied on N equally parts. If the MODE
%   is 't', then  VALUE is a vector containing the t discretization, which
%   is assumed to be regular. IF TEST = 1 when the state of the function
%   RANDN is set to 100 to reproduce the further tests. 
%
%   EXAMPLE
%   T = 1; N_INST = 100; N_WIENER = 3; 
%   DISCRETIZATION.mode = 'h'; DISCRETIZATION.value = 10e-4;
%   TEST = 1;
%   W = do_wiener_vector_mc(T,N_INST, N_WIENER, DISCRETIZATION, TEST);
%
if(exist('test') == 1)
    if (test == 1)
        randn('state', 100);
    end 
end

switch(discretization.mode)
    case('h')
        h = discretization.value; 
        N = ceil(T/h)+1;
        t = 0:h:T;  
    case('N')
        N = discretization.value + 1; 
        h = T/(N-1);
        t = 0:h:T; 
    case('t')
        t = discretization.value; 
        N = length(t); 
        h = T/(N-1); 
    otherwise
        error('DISCRETIZATION MODE ERROR: Discretization mode unknown');
end

dW = zeros(N-1, n_inst,n_wiener); 
dW = sqrt(h)*randn(N-1, n_inst,n_wiener); 
W = cumsum(dW,1); 
W = [zeros(1,n_inst); W];