function [t,y]=test_lin_mult_1d
l = -1;
N = 256;
M = @(t,x) [l 0 0 0 0 0;1 l 0 0 0 0;sin(2*pi*t) 0 l 0 0 0;0 1 0 l 0 0;...
    0 sin(2*pi*t) 1  0 l 0; 0 0 sin(2*pi*t) 0 0 l]*x;
x0 = zeros(6,1);
x0(1) =-0.4326;
[t,y] = ode45(M, 0:1/N:1, x0);
