function test_milstein

assert_set_function('milstein');

a = @(t,x) -x -1;
b = {@(t,x)-x-1}; 
db = {@(t,x) -1};
dt = 0.0001; 
t =  0.0001;
n_inst = 1; 
wold = ones([n_inst, 1]);
xold = 1; 
res = -2 -2*10e-5 + 1-10e-5; 
assert_equals(milstein( a, b, dt, t ,wold, xold, n_inst, db ), res,'sde_1');