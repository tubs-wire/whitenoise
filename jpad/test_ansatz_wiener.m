function test_ansatz_wiener
% TEST_ANSATZ_WIENER tests a series representation of the Wiener process
%
% TEST_ANSATZ_WIENER tests the ansatz function  of the Wiener process based on
% N(0,sqrt(h)) random variables and Heaviside functions. 
%

tspan = [0,1];
n_inst = 1; 
n_wiener = 1;
% Compute on the time span [0,1]  with 100 time steps
discr.mode = 'N'; discr.value = 1000;
test = true;
[Wref,Wref_inc, tref, n_step, h]  = do_wiener_vector_mc(tspan(end) ,n_inst, n_wiener, discr, test);

% Compute the Wiener process with the Heaviside ansatz functions on a N =
% 100 grid

t2 =linspace(0,1,1000); 
W_test = []; 
for i = 1:length(t2)
    W_test(i) = wiener_ansatz(t2(i));
end

plot(tref,[0;Wref])
hold on
plot(linspace(0,1,length(t2)+1), [0,W_test],'g')
hold off
legend('W_{ref}', 'W_{Heaviside}')
xlabel('Time')
ylabel('W(t)')
title('One instance of the standard Wiener process')

  function W = wiener_ansatz(s)
     W = 0;
     for j = 1:max(find(tref < s))-1
         W = W + H(s-tref(j))*Wref_inc(j);
     end
  end
function y = H(x)
          if x >= 0
              y = 1; 
          else
              y = 0;
          end
 end
end