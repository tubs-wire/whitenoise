function [t,y] = ode1(fnc, t, ic)
h = t(2) - t(1); 
y=[]; 
%y(1,:) = ic;
y(:,1) = ic; 
for i = 1:length(t) -  1
%    y(i+1,:)  = y(i,:) + h*feval( fnc, t(i), y(i,:) );
    y(:,i+1) = y(:,1) + h*feval(fnc, t(i), y(:,1));
end