function [avr_pnkt, sigma_sq_pnkt] = examine_wiener(W,t)

length_t = length(t);
rows_W = length_t; 
cols_W = size(W,2);
n_inst = cols_W; 
par_sum = sum(W,2);
avr_pnkt = par_sum/n_inst;
avr_pnkt_mtrx  = repmat(avr_pnkt,1,cols_W);
W_minus_avr_sq = (W - avr_pnkt_mtrx).^2;
par_sum_sq = sum(W_minus_avr_sq, 2);
    
if nargout > 1
    sigma_sq_pnkt = par_sum_sq/n_inst;
end