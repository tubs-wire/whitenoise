function [t,y] = sde_test_add_equation(l, n_step, n_inst ,T, x0, dW)
h = 1/n_step;
t = linspace(0,1, n_step);
a = @(t,x) 0; 
b = {@(t,x) exp(-l*t)};
n_wiener = 1;
% dW = sqrt(h)*randn(n_step, n_inst,n_wiener);
[t, I_f] = sde_classic_solver( @em, a, b, 0, n_step, n_inst, n_wiener, dW, t);
X_0 = repmat(x0, n_step,n_inst);
if (n_inst > 1)
y = repmat(exp(l.*(t)), 1, n_inst).*(X_0 + I_f);
y = [repmat(x0, 1, n_inst); y];
else
    y = exp(l.*(t')).*(X_0 + I_f);
 %   y = [x0; y];
end
%t = [0, t];
if(nargout < 1)
    figure(3)
    plot(t,y)
    title('Exact solution')
end
