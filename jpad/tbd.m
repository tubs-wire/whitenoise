function tbd
clc
clf
set(0,'RecursionLimit',500)
%
% Simulation parameters
%
test = false;
M = [8];
GF = [1];
p = 1; 
n_inst = 1000; 
n_step = 1024;
sde_int = 'em';
%
% SDE assembly
%
ode_fnc ='lin_mult_1d';
x0 = randn(); 
l = -1; 
G0 = @(t) l; 
g0 = @(t) 0; 
g = {@(t) 1};
n_wiener = 1;
tspan = [0,1];
a = @(t_i, x_i) feval(G0,t_i).*x_i + feval(g0, t_i);
b = build_diffusion_coefficients(g, ode_fnc, sde_int);
%
% Create reference Wiener process
%
discr.mode = 'N'; discr.value = n_step;
[Wref, dWref, tref, Nref, href]  = do_wiener_vector_mc(tspan(end) ,n_inst, n_wiener, discr, test);      
switch(sde_int)
    case ('em')
        sde_int_handle = @em;
    case('milstein')
        sde_int_handle = @milstein;
    otherwise
       error('SDE_INT ERROR: Option of sde_int not valid. Choose betweem em or milstein');
end
[t_ref, x_ref] = sde_test_manager(l, Nref, n_inst, tspan(end), x0, ode_fnc, dWref, Wref);
%size_e = [length(GF), n_inst];
%e_w_ref_w_kl = zeros(size_e);
%e_w_ref_w_hat = zeros(size_e);
%e_kl_w_kl = zeros(size_e);
%e_hat_w_hat = zeros(size_e);
%e_hat_kl = zeros(size_e);
for k = 1:length(GF)
%    grb_factor = GF(k); 
%    m = M(k);
  
%    idx = 1:grb_factor:length(tref);
%    t_hat = tref(idx); 
%    W_hat = Wref(idx,:,:);
%    discr_hat.mode = 't'; discr_hat.value = t_hat;
%    n_step_hat = size(W_hat, 1);
 %   for i = 2:n_step_hat 
 %   dW_hat(i-1,:,:) = W_hat(i,:,:) - W_hat(i-1,:,:);
 %   end
    
    % Solution via classic method
  %  [t_cla,x_cla] = sde_classic_solver(sde_int_handle, a, b, x0, n_step_hat, n_inst, n_wiener, dW_hat,t_hat);
    %
    % Wiener KL and sgm solution
    %
   % discr_kl.mode = 't'; discr_kl.value = t_hat;
    rv_sgm = build_sgm_rv(dW_hat, m, n_wiener, t_hat);
    [W_kl, dW_kl, sqrt_lambda, f, t_kl, N_kl, h_kl]=do_wiener_vector_kl(m,tspan,n_inst, n_wiener, discr_kl, rv_sgm);
    W_all.W_hat = W_hat; 
    W_all.rv_hat = dW_hat;
    W_all.t = t_kl; 
    W_all.n = N_kl; 
    W_all.h = h_kl; 
    W_all.sgm = rv_sgm;
    [t_sgm, x_sgm, pce_coef] = sde_sgm_framework(G0, g0, g , tspan, x0 , m, p, n_step, n_inst, sde_int, ode_fnc, W_all);
    [t_hat, x_hat] = sde_test_manager(l, n_step_hat, n_inst ,tspan(end), x0, ode_fnc, dW_hat, W_hat);
    [t_kl, x_kl] = sde_test_manager(l, n_step_hat, n_inst ,tspan(end), x0, ode_fnc, dW_kl, W_kl); 
    e_w_ref_w_kl(k,:) = simpson_rule( abs( x_ref(idx)-x_kl ).^2 );
    e_w_ref_w_hat(k,:) = simpson_rule( abs( x_ref(idx) - x_hat ).^2 );
    e_kl_w_kl(k,:) = simpson_rule( abs( x_sgm - x_kl ).^2 );
    e_hat_w_hat(k,:) = simpson_rule( abs( x_cla - x_hat ).^2 );
    e_hat_kl(k,:) = simpson_rule( abs( x_sgm - x_cla ).^2 );
    fig_H_X = figure(1);
    subplot(2,2,k)
    plot(t_sgm, x_sgm,'r', t_cla, x_cla,'b', t_hat, x_hat,'k.', t_kl, x_kl,'g', t_ref, x_ref,'c :')
    xlabel('Time')
    ylabel('X_t')
    title(sprintf('%d random variables and %d time intervals', M(k),n_step/GF(k)))
    fig_H_W = figure(2);
    subplot(2,2,k)
    plot(t_hat, W_hat, 'k', t_kl, W_kl, 'g')
    xlabel('Time')
    ylabel('W_t')
    title(sprintf('%d random variables and %d time intervals', M(k), n_step/GF(k)))
end
figure(3)
figure('Position', [1,1, 1234,700])
subplot(2,2,1)
loglog(n_step./GF, e_w_ref_w_kl, 'o-')
xlabel('Number of random variables log(m)')
ylabel('log(err)')
title('Error between the analitical solution with fine grid Wiener process and with KL Wiener process')

subplot(2,2,2)
loglog(n_step./GF,e_kl_w_kl, 'o-')
xlabel('Number of random variables (m)')
ylabel('log(err)')
title('Error between the numerical solution  via sGM and analitical with KL Wiener process')

subplot(2,2,3)
loglog(n_step./GF,e_hat_w_hat, 'o-')
xlabel('Number of random variables (m)')
ylabel('log(err)')
title('Error between the numerical solution  via EM and analitical with course grid')

subplot(2,2,4)
loglog(n_step./GF,e_hat_kl, 'o-')
xlabel('Number of random variables (m)')
ylabel('log(err)')
title('Error between the numerical solutions via EM and sGm')
%   print -depsc ...
%    ../../../../latex/document/images/results/sde_errors.eps
figure(fig_H_W)
subplot(2,2,length(GF))
legend('Wiener process with stepwise course approximation', 'Wiener process via KL', 'Location', 'Best')

figure(fig_H_X)
subplot(2,2,length(GF))
legend('X_{sgm}','X_{em}','X_{hat}','X_{tilde}','X_{ref}', 'Location', 'Best')