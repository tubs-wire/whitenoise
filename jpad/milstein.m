function x_new = milstein( a, b,  h, t ,dW, x_old, n_inst, n_wiener)
% MILSTEIN Milstein method for SDEs 
%[T, X, WINC] = MILSTEIN(A, B, DT, T, WINC, XOLD, N_INST,DB) computes  the
%numerical solution of the SDE dX = Adt + BdW, X,  using the Euler-Maruyama
%at step T returning the next T = T+DT to be used and the next Wiener
%process increment WINC. if N_INST > 1 more than one realization are 
%computed.
%
% SEE ALSO EM, SDE_CLASSIC_SOLVER
%
hh = repmat(h, 1, n_inst);
em_part = zeros(size(x_old));
sum_b = zeros(size(x_old));
for i = 1:n_wiener
    sum_b = sum_b + feval(b{1,i},t,x_old).*dW(:,:,i);
end
em_part =  feval(a,t,x_old)*h + sum_b;
if (n_wiener == 1)
    x_new = x_old + em_part + 0.5*feval(b{1,1}, t, x_old)...
        .*feval(b{2,1},t,x_old).*(dW(:,:,1).^2-hh);
else
     I = mult_Ito_integral();
     hot_part = 0; 
     for j1 = 1:n_wiener
         for j2 =1:n_wiener
             hot_part = hot_part +feval(b{1,j1},t,x_old).*feval(b{2,j2}, t, x_old).*...
                 reshape(I(j1,j2,:), 1,[]);
         end
     end
     x_new = x_old + em_part + hot_part; 
end

 function J = mult_Ito_integral(p)
   if( nargin == 0)
      p = 20;
   end
   inv_r = hilb(p); % fast way to compute 1/r
   inv_r = inv_r(1,:); 
   sqrt_rho = sqrt(1/12 - 1/(2*pi*pi)*sum(inv_r.^2));   
   C = (h/(2*pi));
   xi = dW/sqrt(h);
   mu = randn(n_wiener, n_inst);
   eta = randn(n_wiener, p, n_inst);
   zeta = randn(n_wiener, p, n_inst);
   sqrt_2 = sqrt(2);
   J = zeros(n_wiener,n_wiener, n_inst); 
   for j1 = 1:n_wiener
       for j2 = 1:n_wiener
           J(j1, j2, :) = hh.*(0.5*xi(:,:,j1).*...
               xi(:,:,j2) + sqrt_rho*(mu(j1,:).*...
               xi(:,:,j2) - mu(j2,:).*xi(:,:,j1)));
           sum_r = 0; 
           for r = 1:p
               sum_r = sum_r + inv_r(r)*...
                   (reshape(zeta(j1,r,:),1,[]).*...
                   (sqrt_2*xi(:,:,j2)+...
                   reshape(eta(j2,r,:),1,[]))-...
                   reshape(zeta(j2,r,:),1,[]).*...
                   (sqrt_2*xi(:,:,j1)+...
                   reshape(eta(j1,r,:),1,[]))); 
           end
           J(j1,j2,:) =reshape(J(j1,j2,:),1,[])+...
               C*sum_r;
       end
   end
   for i = 1:n_wiener
       J(i,i,:) = (dW(:,:,i).^2 - hh);
   end
 end
end