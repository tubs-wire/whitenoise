function demo_ito_integral
    clc
    N = 500; 
    m = 100;
    n_inst = 1; 
    tspan = [0, 1];
    dt = tspan(2)/N;
    t = linspace(tspan(1),tspan(end), N);
    [t,W] = do_wiener_vector_kl(m,tspan, n_inst,t);
    % We are integrating a Wiener process : INT_0^T WdW = 0.5*W(T)^2 -0.5*T
    % I_f = SUM(from i=1, to n-1) f(t(i))*( W(t(i+1)) - W(t(i) 
    I_f1= sum(W(1:end-1).*W(2:end)) - sum(W(1:end-1).*W(1:end-1));
    I_f_1_exact =  0.5*(W(end)^2-tspan(2))
    err1 = abs(I_f1 - I_f_1_exact)
    % Montecarlo method based on Higham (2001).
    randn('state', 100)
    W = sqrt(dt)*randn(1,N);
    W = cumsum(W);
    I_f2= sum(W(1:end-1).*W(2:end)) - sum(W(1:end-1).*W(1:end-1));
    I_f_2_exact =  0.5*(W(end)^2-tspan(2))
    err2 = abs(I_f2 - I_f_2_exact)