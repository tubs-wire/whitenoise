function [t,y,sde] = sde_test_manager(l, n_step, n_inst ,T, x0, sde_type, dW, W)
% SDE_TEST_MANAGER manages the analitical solution of SDE
% [T,Y] = SDE_MANAGER(L, N_STEP, N_INST, T, X0, SDE_TYPE, dW, W) returns
% the analitical solution of the tests equations (additive or multiplicative) with parameter l using SDE_TYPE 
switch(sde_type)
    case('lin_add_1d')
        [t, y] = sde_test_add_equation(l, n_step, n_inst ,T, x0, dW);
        sde = sprintf('dX_t = %dX_t + dW_t',l);
    case('lin_mult_1d')
      [t,y] = sde_test_mult_equation(l, n_step, n_inst ,T, x0, W);
      sde = sprintf('dX_t = %dX_t + X_tdW_t',l);
    otherwise
        error('SDE TYPE ERROR: incorrect sde type. Choose between lin_add_1d or lin_mult_1d');
end