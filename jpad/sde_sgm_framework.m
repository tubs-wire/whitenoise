function [t, x_sgm, pce_coef] = sde_sgm_framework(G0, g0, g , tspan, x0 , m, p, n_step, n_inst, sde_int, ode_fnc, W_all)
% SDE_SGM_FRAMEWORK  the solution of a SDE using stochastic Galerkin
% method.
% [X_SGM, X_CLA, T] = SDE_SGM_FRAMEWORK(B, G0, g0, B ,TSPAN, X0, M, P,
% N_STEP, N_INST, SDE_INT, ODE_FNC) computes the numerical solution of the
% SDE dX = A(t,X_t)dt + B^(j)(t,X_t)dW^(j) or othewise written dX =
% (G0(t).X_t + g0(t))dt + \SUM_j^d B^j(t,X_t)dW^j on the TSPAN = [ti, tf] with
% initial Conditional X0 via classical methods like Euler Maruyama and
% stochastic Galerkin method. G is a cell of function handles of the d
% diffusion functions . If there is multiplicative noise B^j(t,X_t) =
% G^j(t)X_t, otherwise  B^j(t,X_t) = B^j(t) = G^j(t).G0 and g0 are also
% function handles such that A = G0.X + g0. TSPAN is a column vector of
% size 2 where TSPAN(1) and TSPAN(2) contain the initial and final
% simulation time resp. X0 is the initial condition of the SDE. M is number
% of the random variables in which the SDE is expanded in the PCE. P is the
% degree of the Hermite polynomial of same PCE. N_STEP is the number of
% time steps for the overall simulation. N_INST especifies the number of
% realizations for the solution via classical methods. The type of
% classical method is chosen with SDE_INT which is a string that ONLY takes
% the value 'em' (for Euler-Maruyama method) or 'milstein' (for the
% Milstein scheme). The type of SDE to be solved, i.e., linear additive
% noise,  linear multiplicative noise, or non-linear addtive noise , is
% chosen with ODE_FNC which take the values 'lin_add_1d' (1-d linear
% additive noise), 'lin_mult_1d' (1-d linear multiplicative noise), so far
% only these two options are available. SDE_SGM_FRAMEWORK returns the
% solution of the SDE via stochastic Galerkin method on X_SGM  which is a
% vector of size N_STEP. X_CLA is the solution via classical methods also a
% vector of size N_STEP. [X_SGM, X_CLA, T,ERROR] = SDE_SGM_FRAMEWORK(B, G0,
% g0,B ,TSPAN, X0, M, P, N_STEP, N_INST) returns the mean error between the
% SG method and CLASSICAL ONLY for N_INST > 1. 
%
% 
% EXAMPLE
%
% To solve the SDE dX = (-2X-1)dt - dW for 0 <= t <= 1 and X(0) = 0 with 2
% random variables and  a hermite polynomial of degree 3 for 100 time step
% and 1000 realizations. 
%B = {@(x)-1}; G0 = @(x)-2; g0 = @(x)-1; 
% TSPAN = [0,1]; X0 = 0; M = 2; P = 3; N_STEP = 100; N_INST = 1000; SDE_INT
% = 'em'; ODE_FNC = 'lin_add_1d'; 
%[X_SGM, X_CLA, T] = SDE_SGM_FRAMEWORK(G0, g0, B, TSPAN, X0, M, P, N_STEP,
%N_INST, SDE_INT, ODE_FNC)
%

switch(ode_fnc)
    case 'lin_add_1d'
      ode_fnc = @lin_add_1d;
      lin_mult_flag = false; 
    case 'lin_mult_1d'
      ode_fnc = @lin_mult_1d;
      lin_mult_flag = true;
    otherwise
      error('ODE_FNC ERROR:Option of ode_fnc not valid. Choose betweem lin_add_1d or lin_mult_1d');
end

n_wiener = length(g);

t = W_all.t; 
n_step = W_all.n;  
h = W_all.h; 
rv_sgm = W_all.sgm;

alpha = {};
for i = 1:n_wiener
    alpha{i} = multiindex( m, p );
end
[alpha] = multiindex_combine(alpha, -1);
if(lin_mult_flag)
   I_alpha_k = build_alpha_k(alpha);
end
sizeof_Ialpha = length(alpha);
xi_0 = x0*eye(sizeof_Ialpha, 1); % IC of the ODE solver

n_rv = n_wiener*m; % every wiener process is expanded into  m random variables 

% compute the orthonormal basis w
temp = floor(n_rv/2)+1;
for i = 1:temp
    w(2*(i-1)+1)={@(x)cos(2*pi*(i-1)*x)}; 
    w(2*i) = {@(x)sin(2*i*pi*x)};
end

[t, pce_coef] = ode45(ode_fnc, t, xi_0); 
if(n_wiener > 1) %reorganize in batches of n_wiener. 
                 %See sec. 2.2 Matthies (2005)
     rv_sgm = reshape_rv(rv_sgm, 'D');
end 
 x_sgm = glue_resulting_stochastic_process(pce_coef', rv_sgm, alpha, x0);
 
   function dy = lin_add_1d(t,xi)
      dy = zeros(sizeof_Ialpha,1);    
      dy(:,1) = feval(G0, t).*xi; 
      dy(1,1) =  dy(1,1)+feval(g0,t); 
      for j = 2:m+1
          dy(j,1) = dy(j,1) + feval(w{j-1},t).*feval(g{1}, t);  
      end
    end

%     function dy = lin_mult_1d(t,xi)
%       dy = zeros(sizeof_Ialpha,1);
%       dy(:,1) = feval(G0, t).*xi; 
%       for i = 1:sizeof_Ialpha
%           for k = 1: n_rv
%               idx= I_alpha_k((i-1)*n_rv+k); 
%               if idx ~= 0
%                   dy(i) = dy(i) + feval(w{k},t)*xi(idx)*feval(g{1}, t);
%               end
%           end
%       end
%     end

    function dy = lin_mult_1d(t,xi)
      dy = zeros(sizeof_Ialpha,1);
      dy(:,1) = feval(G0, t).*xi; 
      I = 1:sizeof_Ialpha;
      for k = 1: n_rv
          idx= I_alpha_k((I-1)*n_rv+k);
          I_nonzero=I(idx~=0);
          dy(I_nonzero) = dy(I_nonzero) + feval(w{k},t)*xi(idx(I_nonzero))*feval(g{1}, t);
      end
    end
end
