function rv_sgm = build_sgm_rv(rv_cla, m, n_wiener, t)
% BUILD_SGM_RV compute the random variables for the Hermite polynomials
%
% RV_SGM = BUILD_SGM_RV(RV_CLA, M, N_WIENER, T) computes the
% MxN_INSTxN_WIENER matrix that contains the random variables to later
% evaluate the multivariate Hermite polynomial where M is the number of
% random variables of the PCE, N_INST the number of the sample paths, and
% N_WIENER the number of wiener process of the SDE. T is a vector
% containing the time on this RV_CLA was evaluated.
%
% EXAMPLE
% disc.mode = 't'; disc.value = linspace(0,1); T = 1; n_wiener = 2; 
% n_inst = 5; m = 15; 
% [W, rv_cla, t] = do_wiener_vector_mc(T, n_inst, n_wiener ,discretization);
% rv_sgm = build_sgm_rv(rv_cla, m, n_wiener, t); 
%
% SEE ALSO TRANSFORMATION_MATRIX_RV
%
h = t(2) - t(1);
Q = transformation_matrix_rv(m, h);
rv_sgm = zeros(m, size(rv_cla, 2), n_wiener); 
for k = 1:n_wiener
    rv_sgm(:,:,k) = Q*rv_cla(:,:,k); 
end