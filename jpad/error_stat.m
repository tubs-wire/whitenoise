function [e_mean, delta_e] = error_stat(sim_error, percentage)
% ERROR_STAT mean of simulation error and confidence interval
%
% [E_MEAN, DELTA_E] = ERROR_STAT(SIM_ERROR, PERCENTAGE) computes the expected
% value and the confidence error of the error of simulations. SIM_ERROR is
% a NxM matrix with M simulation batches of N simulation. 
M = size(sim_error,2);   
if M < 2
    error('Number of batches M = 1');
end
if (~exist('percentage'))
    percentage = 0.9;
else
    if (~(percentage < 1 & percentage > 0))
       error('percentage greater than 1 or less than 0.')
    end
end
t = tinv(percentage, M-1); 
e_j_mean = mean(sim_error, 1); 
e_mean = mean(e_j_mean); 
sigma_sq = sum((e_j_mean - repmat(e_mean,1, M)).^2)/(M-1);
delta_e = t*(sqrt(sigma_sq/M));