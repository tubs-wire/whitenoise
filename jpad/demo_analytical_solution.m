function demo_analytical_solution
G0 = 0;
g0 = 0;
gj = 1;
x0 = 0;
[t, x_sol] = analytical_solutions(G0, g0, gj, x0);

if 0
    randn('state',100); 
    partition_size = 100;
    h = 0.0001;
    t = linspace(0,1,partition_size);
    cols_t = size(t,2);
    temp = zeros(zeros(size(t)));
    for i = 2:cols_t
        ts = 0:h:t(i);
        N = length(ts);
        dW = sqrt(h)*rand(1,N);
        W = [0, cumsum(dW)];
        temp(i) = W(end);
    end
    subplot(3,1,1)
    plot(t,x_sol,'r')
    subplot(3,1,2)
    plot(t,temp)
    error = abs(x_sol - temp); 
    subplot(3,1,3)
    plot(t,error)
end
if 0 
    odefnc = @(t,x) G0*x+g0; 
    [t,y] = ode45(odefnc, t, x0);
    error=abs(y-x_sol');
    subplot(2,1,2)
    plot(t,error)
end