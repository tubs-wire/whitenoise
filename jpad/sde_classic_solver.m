function [t,x_sol]= sde_classic_solver( sde_int, a, b, x0, n_step, n_inst, n_wiener, dW, t) 
% SDE_CLASSIC_SOLVER solves a SDE described by the parameters A, B on the
% tspan TSPAN, using the SDE_INT with X0. It returns the times of which 
% the equations is solved T, the vector of the solution X_SOL, and the 
% Wiener increment WINC. If N_INST > 1 more than one realization is computed.  
%
% SEE ALSO EM, MILSTEIN
%
h = (t(end)-t(1))/(n_step-1);
x_sol= x0*ones([1, n_inst]);
xold = x_sol;
xnew = zeros([n_inst, 1]);
for i=2:n_step
   t_i  = t(i);
   xnew = sde_int( a, b, h, t_i ,dW(i-1,:,:), xold, n_inst, n_wiener);
   xold = xnew;
   x_sol = [x_sol; xnew];
end
