function Q = inv_reshape_rv(T, mode, n_wiener); 
% INV_RESHAPE_RV reshape 2-D matrix into a 3-D Matrix according to MODE.
%
% Q = INV_RESHAPE_RV(T, MODE, N_WIENER)  reverses the reshape done by
% function RESHAPE_RV. MODE has to be the same used in function RESHAPE_RV.
% Since  the size of the 1st Dimension of matrix Q  is lost on RESHAPE_INV,
% it must be once again provided. 
%
% EXAMPLE
% Q = randn(3,2,4); 
% MODE = 'D'; 
% [T, thrd_Q] = reshape_rv(Q, mode);
% K = inv_reshape_RV(T, MODE, thrd_Q);
% Q - K
%
rows_q = size(T,1)/n_wiener; 
cols_q = size(T,2); 
Q = zeros(T, rows_q, cols_q, n_wiener);
switch(mode)
    case('U')
        Q = reshape(T, rows_q, cols_q, n_wiener);
    case('D')
        for i = 1:rows_q
            for k = 1:n_wiener
                Q3(i,:,k) = T((k-1)*rows_q + i,:);
            end
        end
    otherwise
        error('MODE ERROR: incorrect mode entered. Mode must be U or D');
end